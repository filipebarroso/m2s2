import 'package:app_francesinha/public/features/home/application/food_service.dart';
import 'package:app_francesinha/public/features/home/data/home_repository.dart';
import 'package:app_francesinha/public/features/home/presentation/pages/home_page.dart';
import 'package:flutter/material.dart';

class RestaurantApp extends StatelessWidget {
  const RestaurantApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Restaurant App',
      theme: ThemeData(
        primarySwatch: Colors.red,
        textTheme: myTextTheme(),
        cardTheme: const CardTheme(
          color: Colors.red,
        ),
      ),
      home: HomePage(
        homeRepository: HomeRepository(FoodService()),
      ),
    );
  }

  TextTheme myTextTheme() {
    return const TextTheme(
      titleLarge: TextStyle(
        fontSize: 24,
        fontWeight: FontWeight.bold,
        color: Colors.black,
      ),
      titleMedium: TextStyle(
        fontSize: 18,
        fontWeight: FontWeight.normal,
        fontStyle: FontStyle.italic,
        color: Colors.grey,
      ),
      displayMedium: TextStyle(
        fontSize: 40,
        fontWeight: FontWeight.bold,
        color: Colors.black,
      ),
    );
  }
}
